const {SERVER_PATH} = require('./utils/constant')
const { BasicProfileRepository: ProfileRepository } = require('../core/build/implementation/BasicProfileRepository')
const { ProfileService } = require('../core/build/service/ProfileService')
console.log(ProfileService)
const profileRepository = new ProfileRepository()

module.exports = new ProfileService(profileRepository) 
