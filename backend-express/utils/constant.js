module.exports = {
    SERVER_PATH: 'http://165.227.134.153:4500',
    MAX_FILE_SIZE: 1024 * 1024 * 5,
    MAX_FILE_UPLOAD: 10,
    PROFILE: {
        MAX_RESUME_COUNT: 5
    }
}
