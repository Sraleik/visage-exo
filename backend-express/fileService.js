const {SERVER_PATH} = require('./utils/constant')
const { FileService } = require('../core/build/service/FileService')
const { BasicFileRepository: FileRepository} = require('../core/build/implementation/BasicFileRepository')
const fileRepository = new FileRepository(`${SERVER_PATH}/file`, `${__dirname}/file`)

module.exports = new FileService(fileRepository)
