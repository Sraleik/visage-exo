const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors')
const app = express();
const uuid  = require("uuid/v4");
const {MAX_FILE_SIZE, MAX_FILE_UPLOAD} = require('./utils/constant')
const fileService = require('./fileService') 
const profileService = require('./profileService') 

app.use(cors()); 
app.use('/file', express.static('file')); 
app.use(fileUpload({}));
app.use(express.json());

const data = { profiles: {}}

async function saveFile(file, maxSize = MAX_FILE_SIZE){
    if(file.size > maxSize){
        return Promise.reject('File size is too large')
    }

    const payload = {
            name: file.name,
            data: file.data,
            hash: file.md5,
            size: file.size,
    }

    return fileService.saveFile(payload)
}

// file upload api
app.post('/upload-files', async (req, res) => {
    if (!req.files) {
        return res.status(500).send({ msg: "File is not found" })
    }

    if(Object.keys(req.files).length > MAX_FILE_UPLOAD){
        return res.status(500).send({ msg: "Too many files" })
    }

    const filePromises = [] 
    
    for(fileKey in req.files) {
        filePromises.push(saveFile(req.files[fileKey]))
    }

    const files = await Promise.all(filePromises).catch((err) =>{ //TODO checker if this catch is usefull
        return res.status(500).send({ msg: "Error occured"})
    })

    return res.send({files})
})

app.post('/profiles', async (req, res) => {
    const {firstName, lastName, resumes } = req.body;

    if(!firstName || !lastName || (resumes.length === 0)) {
        return res.status(500).send({ msg: "Profile is missing a required field" })
    }

    const profile = await profileService.createProfile({firstName, lastName, resumes})

    return res.send({profile})
})

app.get('/profiles', async (req, res) => {
    const profiles = await profileService.getAllProfiles() 
    return res.send(profiles)
})

app.listen(4500, () => {
    console.log('server is running at port 4500');
})
