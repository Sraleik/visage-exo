import { File } from "../model/File"

export interface IFileRepository {
    saveFile(file: File): Promise<File>
}