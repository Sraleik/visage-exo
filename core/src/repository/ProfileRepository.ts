import { Profile } from "../model/Profile";

export interface IProfileRepository {
    createProfile(profile: Profile): Promise<Profile>
    getAllProfiles(): Promise<Profile[]>
}