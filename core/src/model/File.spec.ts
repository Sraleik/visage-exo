import { File } from './File'

describe('File', () => {
    test('create a file with path', () => {
        const params = {
            name: 'image.jpg', 
            hash: 'e9786880',
            path: 'http://example.com/32.jpg',
            size: 5648 
        }
        const file = File.create(params);

        expect(!!file.id).toBe(true);
        expect(file.name).toBe(params.name);
        expect(file.hash).toBe(params.hash);
        expect(file.path).toBe(params.path);
        expect(file.size).toBe(params.size);
    });

    test('create a file with Buffer', () => {
        const params = {
            name: 'image.jpg', 
            hash: 'e9786880',
            data: new Buffer('osef'),
            size: 5648 
        }
        const file = File.create(params);

        expect(!!file.id).toBe(true);
        expect(file.name).toBe(params.name);
        expect(file.hash).toBe(params.hash);
        expect(file.data).toBe(params.data);
        expect(file.size).toBe(params.size);
    })

    test('create a file without Buffer or Path', () => {
        const params = {
            name: 'image.jpg', 
            hash: 'e9786880',
            size: 5648 
        }

        expect(() => File.create(params)).toThrow();
    })

})