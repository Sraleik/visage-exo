import {v4 as uuid } from 'uuid'

interface FileInput {
    id: string,
    name: string,
    hash: string,
    size: number,
    data?: Buffer,
    path?: string
}

export interface FileCreation {
    name: string,
    hash: string,
    size: number,
    data?: Buffer,
    path?: string
}

export class File {
    public id: string
    public name: string
    public hash: string
    public size: number 
    public data?: Buffer 
    public path?: string

    static create(payload: FileCreation){
        const {name, hash, size, data, path} = payload

        return new File({id: uuid(), name, hash, size, data, path})
    }

    constructor(payload: FileInput){
        if(!payload.data && !payload.path) {
            throw new Error("File need data or a path")
        }

        this.id = payload.id
        this.name = payload.name 
        this.hash = payload.hash 
        this.size = payload.size
        this.data = payload.data
        this.path = payload.path
    }
}