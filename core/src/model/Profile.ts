import {v4 as uuid } from 'uuid'
import { File } from "./File";

interface ProfileInput {
    id: string,
    firstName: string,
    lastName: string,
    resumes?: [File]
}

export interface ProfileCreation {
    firstName: string,
    lastName: string,
    resumes?: [File]
}
export class Profile {
    public id: string
    public firstName: string
    public lastName: string
    public resumes?: [File] | []
    public createdAt: Date
    public updatedAt: Date

    static create(payload: ProfileCreation){
        return new Profile({id: uuid(), ...payload})        
    }

    constructor(payload: ProfileInput){
        const {id, firstName, lastName, resumes} = payload
        this.id = id 
        this.firstName = firstName
        this.lastName = lastName
        this.resumes = resumes || []
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }
}