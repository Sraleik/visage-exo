import { Profile } from './Profile'
test('create a new profile', () => {
    const params = {
        firstName: 'John',
        lastName: 'Wick'
    }

    const profile = Profile.create(params);
    expect(!!profile.id).toBe(true);
});