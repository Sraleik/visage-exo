import { IFileRepository } from "../repository/FileRepository";
import { File } from "../model/File";
import fs from "fs";
import { promisify } from 'util';

const writeFile = promisify(fs.writeFile);

// This is a simple way to store the file, a AwsFileRepository could be implemented 
export class BasicFileRepository implements IFileRepository {
    constructor(public fileFolderUrl: string, public folderPath: string){}

    async saveFile(file: File) {
        if(!file.data) {
            throw new Error("File has no data")
        }

        //I use the hash as filename to not store identical file only once 
        await writeFile(`${this.folderPath}/${file.hash}`, file.data)
        file.path = `${this.fileFolderUrl}/${file.hash}`
        return file
    }

}