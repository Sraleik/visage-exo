import { IProfileRepository} from "../repository/ProfileRepository";
import { Profile } from "../model/Profile";

// This is a very basic way of storing the profile. For more advance storage, a BddProfileRepository could be implemented.
export class BasicProfileRepository implements IProfileRepository {
    constructor(private store: any = { profiles: {}}){}

    createProfile(profile: Profile): Promise<Profile> {
        this.store.profiles[profile.id] = profile
        console.log(this.store)
        return Promise.resolve(profile)
    }

    getAllProfiles(): Promise<Profile[]> {
        const sortedProfiles =  Object.values(this.store.profiles)
                                        .sort((a, b) => {
                                            return (<Profile>b).createdAt.getTime() - (<Profile>a).createdAt.getTime()  
                                        })

        return Promise.resolve(<Profile[]>sortedProfiles)
    }
}