import {File, FileCreation} from '../model/File'
import { IFileRepository } from '../repository/FileRepository';

export class FileService {
    constructor(public repository: IFileRepository){}

    async saveFile(payload: FileCreation){
        const file = File.create(payload)
        await this.repository.saveFile(file)
        return file;
    }
}