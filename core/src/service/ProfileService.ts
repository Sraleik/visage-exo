import {Profile, ProfileCreation} from '../model/Profile'
import { IProfileRepository } from '../repository/ProfileRepository';

export class ProfileService {
    constructor(public repository: IProfileRepository){}

    async createProfile(payload: ProfileCreation){
        const profile = Profile.create(payload)
        await this.repository.createProfile(profile)
        return profile;
    }

    getAllProfiles(){
        return this.repository.getAllProfiles()
    }
}