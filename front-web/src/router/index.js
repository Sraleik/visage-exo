import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Profiles from "../views/Profiles.vue";
import SubmitProfile from "../views/SubmitProfile.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/profiles",
    name: "profiles",
    component: Profiles
  },
  {
    path: "/submit-profile",
    name: "submit-profile",
    component: SubmitProfile
  }
];

const router = new VueRouter({
  routes
});

export default router;
