import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import VueComp from "@vue/composition-api";
import { VuelidatePlugin } from "@vuelidate/core";
import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css";

Vue.config.productionTip = false;
Vue.use(VueComp);
Vue.use(VuelidatePlugin);
Vue.use(VueToast, { position: "top-left" });

Vue.filter("formatDate", function(value) {
  if (value) {
    return new Intl.DateTimeFormat("en-GB").format(new Date(value));
  }
});

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount("#app");
