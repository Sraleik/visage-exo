import { ref } from "@vue/composition-api";
import axios from "axios";

const backendUrl = "http://165.227.134.153:4500"; // Todo move this somewhere else

export function useGetProfiles() {
  const profiles = ref([]);

  const getProfiles = async () => {
    const res = await axios.get(`${backendUrl}/profiles`);
    profiles.value = res.data;
  };

  return [profiles, getProfiles];
}
