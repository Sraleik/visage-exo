import { computed } from "@vue/composition-api";
import { useVuelidate } from "@vuelidate/core";
import { required, minLength, maxLength } from "@vuelidate/validators";

import { PROFILE } from "@/utils/constant";

export function useProfileValidators(propsToValidate) {
  const containFile = (min, max) => {
    return value => {
      return value.length >= min && value.length <= max;
    };
  };

  const rules = {
    firstName: {
      required,
      minLength: minLength(PROFILE.MIN_FIRSTNAME_LENGTH),
      maxLength: maxLength(PROFILE.MAX_FIRSTNAME_LENGTH)
    },
    lastName: {
      required,
      minLength: minLength(PROFILE.MIN_LASTNAME_LENGTH),
      maxLength: maxLength(PROFILE.MAX_LASTNAME_LENGTH)
    },
    resumes: {
      containFile: containFile(
        PROFILE.MIN_RESUME_COUNT,
        PROFILE.MAX_RESUME_COUNT
      )
    }
  };

  const $v = useVuelidate(rules, { ...propsToValidate });
  const validationError = function(key, customMessage) {
    return () => {
      const firstError = $v.value[key].$errors[0];
      if (firstError) {
        return customMessage ? customMessage : firstError.$message;
      }
      return;
    };
  };

  const firstNameError = computed(validationError("firstName"));
  const lastNameError = computed(validationError("lastName"));
  const resumesError = computed(
    validationError("resumes", "You must send between 1 and 5 files")
  );

  return [$v, firstNameError, lastNameError, resumesError];
}
