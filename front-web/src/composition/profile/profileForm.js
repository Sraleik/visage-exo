import { ref } from "@vue/composition-api";
import { useUploadFiles } from "@/composition/file/upload";
import { useProfileValidators } from "./validators";
import axios from "axios";
import { v4 as uuid } from "uuid";

const backendUrl = "http://165.227.134.153:4500"; // Todo move this somewhere else
const fileStorageUrl = "http://165.227.134.153:4500"; // Todo move this somewhere else

export function useProfileForm() {
  const firstName = ref("");
  const lastName = ref("");
  const [resumes, uploadResumes] = useUploadFiles(fileStorageUrl);
  const [
    $v,
    firstNameError,
    lastNameError,
    resumesError
  ] = useProfileValidators({ firstName, lastName, resumes });

  const sendProfile = async () => {
    const updloadedResume = await uploadResumes();
    const profile = {
      firstName: firstName.value,
      lastName: lastName.value,
      resumes: updloadedResume.map(resume => {
        return {
          id: uuid(),
          fileName: resume.name,
          filePath: resume.path,
          hash: resume.hash,
          size: resume.size
        };
      })
    };

    return axios
      .post(`${backendUrl}/profiles`, profile)
      .then(function(res) {
        return res.data.files;
      })
      .catch(function(error) {
        console.log(error.response);
      });
  };

  const resetProfile = () => {
    firstName.value = "";
    lastName.value = "";
    resumes.value = [];
    $v.value.$reset();
  };

  return {
    firstName,
    lastName,
    resumes,
    sendProfile,
    resetProfile,
    $v,
    firstNameError,
    lastNameError,
    resumesError
  };
}
