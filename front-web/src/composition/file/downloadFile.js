import axios from "axios";

export const downloadFile = (filePath, fileName) => {
  axios.get(filePath, { responseType: "blob" }).then(response => {
    const fileURL = window.URL.createObjectURL(new Blob([response.data]));
    const fileLink = document.createElement("a");

    fileLink.href = fileURL;
    fileLink.setAttribute("download", fileName);
    document.body.appendChild(fileLink);

    fileLink.click();
  });
};
