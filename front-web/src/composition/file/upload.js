import { ref, computed } from "@vue/composition-api";
import axios from "axios";

export function useUploadFiles(backendUrl) {
  const files = ref([]);
  const uploadErrors = computed(function() {
    const res = [];
    files.value.forEach(file => {
      if (file.size > 2 * 1024 * 1024) {
        res.push({ message: `${file.name} is too large` });
      }
    });

    if (files.value.length === 0) {
      res.push({ message: "No file" });
    } else if (files.value.length > 5) {
      res.push({ message: "Too many files" });
    }

    return res;
  });

  const canUpload = computed(() => !(uploadErrors.value.length > 0));

  const uploadFiles = function() {
    if (!canUpload.value) return Promise.reject(uploadErrors.value);
    let formData = new FormData();
    files.value.forEach((file, index) => {
      formData.append(`file-${index}`, file);
    });

    return axios
      .post(`${backendUrl}/upload-files`, formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then(function(res) {
        return res.data.files;
      });
  };

  return [files, uploadFiles, uploadErrors];
}
