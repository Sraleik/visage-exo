# Visage Exo 

## Project setup

```
yarn install
```
It is a monorepo so one yarn install should be enough for the 3 folder

## Core

Core where the domain code is. It should be agnostic of implementation.

```
yarn run build
```
So the rest api can import what it need

## Bakend-express

Is the REST api use the Core domain

```
yarn run server
```

## Front-web

Is a vue front-end using the Express api

```
yarn run serve
```

## Local dev

``` 
git checkout dev
```

I did not use environement variable to set the server url :(

